const STRAPI_BEARER_TOKEN="192136d9948e9480f4254aafddaf5d668f9e7378fd31d07de362a411d21aaff3c408c6b2ede59c0dca1915b6ac72fb64ca76e98dfb4083fca17966f9cf58270174492d0f9327c05d4537b34b82d17f6a551f08459a4a7f3f16061b0e7fa0cfb63eb37ede1467625797c909475e65e60218e15e5177bab3270102285c00d24fb7";
export default {
  /**
   * Call an api endpoint every minute
   */
  "*/13 * * * *": async ({ strapi }) => {    
    fetch('https://strapi-test-postgreonvercel.onrender.com/admin/', {
      headers: {Authorization: `Bearer ${STRAPI_BEARER_TOKEN}`}
    })
    .then(resp => console.log('server pinged'))
    .catch(err => console.log('ping error'))
  }
};