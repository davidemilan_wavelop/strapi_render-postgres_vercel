// doc per i lifecycle
// https://docs.strapi.io/dev-docs/backend-customization/models#available-lifecycle-events

export default {
  beforeCreate(event) {
    let { data, where, select, populate } = event.params;

    data.isTableFull = data.numOfPeople === 4;
  },

  afterCreate(event) {
    revalidateContent(event.result.id);
  },
  afterUpdate(event) {
    revalidateContent(event.result.id);
  },

};

async function revalidateContent(id: string){
  // revalida la singola pagina dedicata ai computer (con url "/:id" in questo caso)
  await fetch(`https://next-cron-and-cms-test.vercel.app/api/revalidate?path=/${id}&secret=revalidationToken`).then(res => console.log(res));
  
  //bisognerebbe rivalidare anche le pagine in cui è possibile che si veda l'elemento modificato:
  // ad es. : di un articolo viene mostrato il titolo nel feed della home page, vado a revalidare anche la homepage
}