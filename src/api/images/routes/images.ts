export default {
  routes: [
    {
     method: 'GET',
     path: '/images',
     handler: 'images.getAllImageUrls',
     config: {
       policies: [],
       middlewares: [],
     },
    },
  ],
};
