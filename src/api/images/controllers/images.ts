const BASE_PATH = "https://strapi-test-postgreonvercel.onrender.com"
const STRAPI_BEARER_TOKEN = "192136d9948e9480f4254aafddaf5d668f9e7378fd31d07de362a411d21aaff3c408c6b2ede59c0dca1915b6ac72fb64ca76e98dfb4083fca17966f9cf58270174492d0f9327c05d4537b34b82d17f6a551f08459a4a7f3f16061b0e7fa0cfb63eb37ede1467625797c909475e65e60218e15e5177bab3270102285c00d24fb7";

export default {
  async getAllImageUrls(ctx) {
    const {rows} : {rows: {url: string}[]} = await strapi.db.connection.raw(
      `select Url from files left join files_related_morphs on files.Id = Related_id`
  );
    const urls : string[] = rows.map((urlObj : {url : string}) => urlObj.url);
    ctx.body = urls;
  },
 };